<div id="win_r">
    <form action="<?php echo site_url('admin/user_doedit'); ?>" method="post">
        <ul>
            <li>
                <span class="title"><?php echo lang('username'); ?></span>
                <?php echo $user->username; ?>
                <input type="hidden" name="uid" value="<?php echo $user->id; ?>">
                <input type="hidden" name="username" value="<?php echo $user->username; ?>">
            </li> 
            <li>
                <span class="title"><?php echo lang('password'); ?></span>
                <input type="password" name="password" class="input" maxlength="20" />
                <span class="m_left_10 notice"><?php echo lang('edit_password_rule_note'); ?></span>
            </li> 
            <li>
                <span class="title"><?php echo lang('re_password'); ?></span>
                <input type="password" name="re_password" class="input" maxlength="20" />
            </li> 
            <li>
                <span class="title"><?php echo lang('authority'); ?></span>
                <select name="role" class="input">
                    <?php
                    if ('admin' == strval($user->group)) {
                        ?>
                        <option value="0"><?php echo lang('common_user'); ?></option>
                        <option value="1" selected="selected"><?php echo lang('administrator'); ?></option>
                        <?php
                    } else {
                        ?>
                        <option value="0" selected="selected"><?php echo lang('common_user'); ?></option>
                        <option value="1"><?php echo lang('adminitrator'); ?></option>
                        <?php
                    }
                    ?>
                </select>
            </li> 
            <li class="text_c">
                <input type="submit" value="<?php echo lang('edit'); ?>" onclick="submit();" />
            </li> 
        </ul>
    </form>
</div>