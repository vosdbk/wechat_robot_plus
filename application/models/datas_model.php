<?php

/**
 * Description of datas_model
 *
 * @author terminus
 */
class Datas_model extends CI_Model {

    private $tableName = 'datas';

    public function __construct() {
        parent::__construct();
    }

    public function getAll($only_count = 0, $limit = '', $offset = '') {
        if (1 != $only_count) {
            if ($limit > 0) {
                $this->db->limit($limit);
                if ($offset > 0) {
                    $this->db->limit($limit, $offset);
                }
            }
        }
        $result = $this->db->get($this->tableName);
        if ($result->num_rows() > 0) {
            if (1 == $only_count) {
                return $result->num_rows();
            }
            return $result->result();
        }
        return FALSE;
    }

    public function get($user_opn_id) {
        $where['user_opn_id'] = $user_opn_id;

        $result = $this->db->get_where($this->tableName, $where, 1);
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return FALSE;
    }

    public function add($user_opn_id, $last_cmd = '', $last_cmd_id = 0, $confirm = 0, $data = '') {
        if ($this->get($user_opn_id)) {
            return FALSE;
        }

        $set = array(
            'user_opn_id' => $user_opn_id,
            'last_cmd' => $last_cmd,
            'last_cmd_id' => $last_cmd_id,
            'confirm' => $confirm,
            'data' => $data,
            'last_time' => time()
        );

        $this->db->insert($this->tableName, $set);
        $id = $this->db->insert_id();
        if ($id > 0) {
            return $id;
        }
        return FALSE;
    }

    public function edit($user_opn_id, $last_cmd = '', $last_cmd_id = 0, $data = '', $confirm = 0) {
        $where['user_opn_id'] = $user_opn_id;

        $set = array(
            'last_cmd' => $last_cmd,
            'last_cmd_id' => $last_cmd_id,
            'data' => $data,
            'confirm' => $confirm,
            'last_time' => time()
        );

        if (TRUE === $this->db->update($this->tableName, $set, $where, 1)) {
            return TRUE;
        }
        return FALSE;
    }

    public function del($user_opn_id) {
        $where['user_opn_id'] = $user_opn_id;

        $this->db->delete($this->tableName, $where, 1);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

}
